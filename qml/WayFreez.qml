import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import Ubuntu.Components.Popups 1.3

Page {
    id: wayfreez
    header: PageHeader {
        id: wayfreezHeader
        title: i18n.tr("Waydroid container tool")
        opacity: 1
        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr("About")
                    onTriggered: {pageStack.push(aboutpage)}
                    iconName: "info"
                },
                Action {
                    text: i18n.tr("Settings")
                    onTriggered: {pageStack.push(settingspage)}
                    iconName: "settings"
                }
            ]
        }
    } //PageHeader

/*    property bool completed: false
    property var freezeCount:""
    property var unfreezeCount:""*/
    property var waydroidstatus: "unChecked"
    property var waydroidstatuslbl: waydroidstatus
    property var statusflick:""

    function checkOperation() {
//        lblbutton.text = ""
        python.call('containeroperation.checkState', [], (returnValue) => {
            if (returnValue.includes('FROZEN'))
                waydroidstatus = returnValue;
            if (returnValue.includes('not found'))
                waydroidstatus = "NOT INSTALLED";
            if (returnValue.includes('RUNNING'))
                waydroidstatus = returnValue;
            if (returnValue === '')
                waydroidstatus = "STOPPED";
            waydroidstatuslbl = waydroidstatus;
//            console.log('checkOperation.containeroper.checkState: ' + returnValue);
        });
        destroyAnimation.start();
    }

    function startOperation(password, shellHistory) {
        lbloperation.text = "";
        if (waydroidstatus === "RUNNING")
            python.call('containeroperation.freeze', [password, shellHistory]),
            waydroidstatus = "FROZEN";
        else
            python.call('containeroperation.unfreeze', [password, shellHistory]),
            waydroidstatus = "RUNNING";
    }

    function waydroidOperation(shellHistory) {
        python.call('containeroperation.wayShowFullUi', [shellHistory]),
        destroyAnimation.start();
    }

    function showPasswordPrompt() {
        if (root.inputMethodHints === null) {
            startOperation('');
            return;
        }
        PopupUtils.open(passwordPrompt);
    }

    function delay(delayTime,cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }

    MainView {
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        Timer {
            id: timer
        }

        ListItem {
            id: lstItemStatus
            divider.visible: true
            color: {
                if (waydroidstatuslbl == "FROZEN")
                    color: "#5ebbfb"
                else if (waydroidstatuslbl == "RUNNING")
                    color: "#faaf03"
                else color: "transparent"
            }
        }

        Label {
            id: lblstatus
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: lstItemStatus.verticalCenter
            width: parent.width / 1.1
            horizontalAlignment: Text.AlignHCenter
            text: i18n.tr("Container is : " + waydroidstatuslbl)
            font.pointSize: units.gu(2)
            wrapMode: Text.Wrap
        }

        Label {
            id: lblflick
            anchors.top: lstItemStatus.bottom
            anchors.topMargin: parent.height / 12
            anchors.horizontalCenter: lstItemStatus.horizontalCenter
            //width: parent.width / 1.1
            font.pointSize: units.gu(1.6)
            color: theme.palette.normal.positive
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            text: statusflick

            NumberAnimation on opacity {
                id: destroyAnimationFlick
                from: 1
                to: 0
                duration: 2500
            }

            NumberAnimation on opacity {
                id: createAnimationFlick
                from: 0
                to: 1
                duration: 100
            }
        }

        Flickable {
            id: flickArea
            anchors {
                top: lblstatus.bottom
                topMargin: parent.height / 4
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            flickableDirection: Flickable.VerticalFlick
            //Prevent flick when running -> prevent multiple refresh
            interactive: !activity.running ? true : false

            onFlickStarted: {
                activity.running = true;
                checkOperation();
            }

            onMovementStarted: {
                createAnimationFlick.start();
                statusflick = "Release to refresh";
            }

            onMovementEnded: {
                statusflick = "Pull to refresh";
                destroyAnimationFlick.start();
            }

            Label {
                id: lblbutton
//                anchors.top: lblstatus.top
                anchors.topMargin: parent.height / 4

                x: units.gu(2)
                width: parent.width / 2
                font.pointSize: units.gu(1.6)
                wrapMode: Text.Wrap
                text: waydroidstatus == "unChecked" ? ""
                    : waydroidstatus == "NOT INSTALLED" ? i18n.tr("Install Waydroid first...")
                    : waydroidstatus == "STOPPED" ? i18n.tr("Start Waydroid first...")
                    : i18n.tr("Press button to operate Waydroid")
            }

            Button {
                id: operButton
                anchors.right: parent.right
                anchors.rightMargin: units.gu(2)
                anchors.verticalCenter: lblbutton.verticalCenter
                anchors.topMargin: parent.height / 12.5
                enabled: !activity.running
                visible: waydroidstatus == "STOPPED"
                    || waydroidstatus == "NOT INSTALLED"
                    || waydroidstatus == "unChecked" ? false : true
                color: theme.palette.normal.positive
                text: waydroidstatus == "FROZEN" ? i18n.tr("Unfreeze") : i18n.tr("Freeze")
                onClicked: {
                    if (pwdlessUnfreezeEnabled == '0')
                        showPasswordPrompt();
                    else if (waydroidstatus == "FROZEN")
                        lbloperation.text = "",
                        createAnimation.start(),
                        activity.running = true,
                        waydroidOperation(shellHistory),
                        waydroidstatus = "RUNNING";
                    else showPasswordPrompt();
    //                PopupUtils.open(passwordPrompt)
                    return;
    //                pageStack.pop();

    /*test
                    if (password == "")
                        showPasswordPrompt(),
                        password = 1;
                    else startOperation('');
                //                PopupUtils.open(passwordPrompt)
                    return;

    */
                }
            }

            Label {
                id: lbloperation
                anchors.top: operButton.bottom
                anchors.topMargin: parent.height / 8
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width / 1.5
                horizontalAlignment: Text.AlignHCenter
                text: ""
                font.pointSize: units.gu(1.6)
                wrapMode: Text.Wrap

                NumberAnimation on opacity {
                    id: destroyAnimation
                    from: 5
                    to: 0
                    duration: 5000
                }

                NumberAnimation on opacity {
                    id: createAnimation
                    from: 0
                    to: 1
                    duration: 100
                }
            }

            ActivityIndicator {
                id: activity
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: parent.height / 7.5
                running: true
            }

        } //Flickable
    } //MainView

    Component {
        id: passwordPrompt
        PasswordPrompt {
            onPassword: {
/*                console.log('waydroid status avt oper: ' + waydroidstatus);
                console.log('waydroid status lbl avt oper: ' + waydroidstatuslbl);*/
                lbloperation.text = "";
                createAnimation.start();
                startOperation(password, shellHistory);
                activity.running = true;
                destroyAnimation.start();
/*                console.log('waydroid status apr oper: ' + waydroidstatus);
                console.log('waydroid status lbl apr oper: ' + waydroidstatuslbl);*/
            }
            onCancel: {
                activity.running = false;
            }
        }
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));

            importNames('containeroperation', ['containeroperation'], () => {
                //Exec.call
                python.call('containeroperation.checkState', [], (returnValue) => {
                    if (returnValue.includes('FROZEN'))
                        waydroidstatus = returnValue;
                    if (returnValue.includes('not found'))
                        waydroidstatus = "NOT INSTALLED";
                    if (returnValue.includes('RUNNING'))
                        waydroidstatus = returnValue;
                    if (returnValue === '')
                        waydroidstatus = "STOPPED";
                    waydroidstatuslbl = waydroidstatus;
//                    console.log('ComponCompl.containeroper.checkState: ' + returnValue);
                });
                //Exec.onCompleted
                python.setHandler('whatOperState', (operState) => {
                    lbloperation.text = operState;
                });

                python.setHandler('whatWayStatus', (wayStatus) => {
                    lbloperation.text = wayStatus;
                    waydroidstatuslbl = waydroidstatus;
                    activity.running = false;
                });
            });
            destroyAnimation.start();

/*            importNames('historyoperation', ['historyoperation'], () => {

                python.call('historyoperation.freezeCountHistory', [], (returnValue) => {
/*                    freezeCount = returnValue;
                    freezeCountlbl = freezeCount;
                    console.log('freezeCountHistory: ' + returnValue);
                });

                python.call('historyoperation.unfreezeCountHistory', [], (returnValue) => {
/*                    freezeCount = returnValue;
                    freezeCountlbl = freezeCount;
                    console.log('unfreezeCountHistory: ' + returnValue);
                });

                python.call('historyoperation.sudoCountHistory', [], (returnValue) => {
/*                    freezeCount = returnValue;
                    freezeCountlbl = freezeCount;
                    console.log('sudoCountHistory: ' + returnValue);
                });

/*                python.setHandler('whatOperState', (operState) => {
                    lbloperation.text = operState;
                });

                python.setHandler('whatWayStatus', (wayStatus) => {
                    lbloperation.text = wayStatus;
                    waydroidstatuslbl = waydroidstatus;
                    activity.running = false;
                });

            });*/
        }

        onError: {
            console.log('python error:', traceback);
        }
    }
}
