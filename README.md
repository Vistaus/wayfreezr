# WayFreezr

Waydroid container freeze and unfreeze.

A simple tool that allows to freeze and unfreeze WayDroid's container on Ubuntu Touch.
Freezing the container will save battery.

## License

Copyright (C) 2023  wilfridd

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
