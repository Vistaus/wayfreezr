# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the wayfreezr.wilfridd package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: wayfreezr.wilfridd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-13 20:57+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/AboutPage.qml:8 ../qml/WayFreez.qml:17
msgid "About"
msgstr ""

#: ../qml/AboutPage.qml:39
msgid "↶ WayFreezr ↷"
msgstr ""

#: ../qml/AboutPage.qml:57
msgid "Source"
msgstr ""

#: ../qml/AboutPage.qml:57
msgid "Version: "
msgstr ""

#: ../qml/AboutPage.qml:67
msgid ""
"A simple tool that allows to freeze and unfreeze WayDroid's container on "
"Ubuntu Touch."
msgstr ""

#. TRANSLATORS: Change the URL according to the language
#: ../qml/AboutPage.qml:76
msgid ""
"This program is free software under the terms of the <a href='https://www."
"gnu.org/licenses/gpl-3.0.en.html'>GNU General Public License.</a>"
msgstr ""

#: ../qml/AboutPage.qml:84
msgid "Copyright"
msgstr ""

#: ../qml/AboutPage.qml:94
msgid "DONATE"
msgstr ""

#: ../qml/PasswordPrompt.qml:8
msgid "Authorization"
msgstr ""

#: ../qml/PasswordPrompt.qml:33
msgid "Enter your passcode:"
msgstr ""

#: ../qml/PasswordPrompt.qml:33
msgid "Enter your password:"
msgstr ""

#: ../qml/PasswordPrompt.qml:40
msgid "passcode"
msgstr ""

#: ../qml/PasswordPrompt.qml:40
msgid "password"
msgstr ""

#: ../qml/PasswordPrompt.qml:54
msgid "Incorrect passcode"
msgstr ""

#: ../qml/PasswordPrompt.qml:54
msgid "Incorrect password"
msgstr ""

#: ../qml/PasswordPrompt.qml:59
msgid "Ok"
msgstr ""

#: ../qml/PasswordPrompt.qml:79
msgid "Cancel"
msgstr ""

#: ../qml/SettingsPage.qml:9 ../qml/WayFreez.qml:22
msgid "Settings"
msgstr ""

#: ../qml/SettingsPage.qml:38
msgid "Theme"
msgstr ""

#: ../qml/SettingsPage.qml:49
msgid "Force dark mode"
msgstr ""

#: ../qml/WayFreez.qml:12
msgid "Waydroid container tool"
msgstr ""

#: ../qml/WayFreez.qml:114
msgid "Container is : "
msgstr ""

#: ../qml/WayFreez.qml:185
msgid "Install Waydroid first..."
msgstr ""

#: ../qml/WayFreez.qml:186
msgid "Start Waydroid first..."
msgstr ""

#: ../qml/WayFreez.qml:187
msgid "Press button to operate Waydroid"
msgstr ""

#: ../qml/WayFreez.qml:201
msgid "Freeze"
msgstr ""

#: ../qml/WayFreez.qml:201
msgid "Unfreeze"
msgstr ""

#: wayfreezr.desktop.in.h:1
msgid "WayFreezr"
msgstr ""
